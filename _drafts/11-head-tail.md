---
### `tail` and `head`--- Output the lines at the bottom of a file

The `tail` command is used to display lines at the bottom of a file. 
By itself, `tail` lists the bottom 10 lines. 

Let's try for ourselves:
```bash
$ tail 1998.dat
```
```
1998/12/914438100.19914.txt|159.226.5.151|CN|China
1998/12/914561519.28497.txt|209.149.111.45|US|United States
1998/12/914690993.5712.txt|202.84.12.129|CN|China
1998/12/914945890.7710.txt|199.174.210.45|US|United States
1998/12/914949946.7826.txt|203.120.165.212|SG|Singapore
1998/12/914950939.7970.txt|198.54.223.1|ZA|South Africa
1998/12/914951833.7981.txt|204.177.236.127|US|United States
1998/12/915115558.12813.txt|206.175.96.141|US|United States
1998/12/915115559.12813.txt|206.175.96.141|US|United States
1998/12/915115560.12813.txt|206.175.101.93|US|United States
```
{: .output}
This file is in chronological order so you can see the date stamps at the 
bottom of the file.

We can also specify how many lines we would like to see with the `-n` switch.

Here we can see the bottom 20 lines of the file:
```bash
$ tail -n 20 1998.dat
```
```
1998/12/914191895.7568.txt|198.81.17.73|US|United States
1998/12/914191896.7568.txt|206.212.231.88|US|United States
1998/12/914191897.7568.txt|152.205.143.78|CO|Colombia
1998/12/914207414.665.txt|198.169.201.2|CA|Canada
1998/12/914400880.19244.txt|198.214.72.1|US|United States
1998/12/914400881.19244.txt|209.154.87.55|US|United States
1998/12/914400882.19244.txt|193.175.112.2|DE|Germany
1998/12/914400883.19244.txt|198.214.72.1|US|United States
1998/12/914400884.19244.txt|152.204.151.62|CO|Colombia
1998/12/914400885.19244.txt|159.226.5.151|CN|China
1998/12/914438100.19914.txt|159.226.5.151|CN|China
1998/12/914561519.28497.txt|209.149.111.45|US|United States
1998/12/914690993.5712.txt|202.84.12.129|CN|China
1998/12/914945890.7710.txt|199.174.210.45|US|United States
1998/12/914949946.7826.txt|203.120.165.212|SG|Singapore
1998/12/914950939.7970.txt|198.54.223.1|ZA|South Africa
1998/12/914951833.7981.txt|204.177.236.127|US|United States
1998/12/915115558.12813.txt|206.175.96.141|US|United States
1998/12/915115559.12813.txt|206.175.96.141|US|United States
1998/12/915115560.12813.txt|206.175.101.93|US|United States
```
{: .output}


Alternatively we can use the `head` command to see the lines at the 
begining of the file. 

Follow along with this example:
```bash
$ head 1998.dat
```
```
1998/03/890929468.24864.txt|204.31.253.89|US|United States
1998/03/890929472.24865.txt|153.37.75.113|CN|China
1998/03/890929475.24866.txt|153.37.88.4|CN|China
1998/03/890929479.24867.txt||Fail to get source IP|
1998/03/890929482.24868.txt|153.36.90.245|CN|China
1998/03/890929485.24869.txt|209.84.113.62|US|United States
1998/03/890929489.24870.txt|153.37.97.151|CN|China
1998/03/890929492.24871.txt|198.81.17.36|US|United States
1998/03/890929496.24872.txt|198.81.17.41|US|United States
1998/03/890929499.24873.txt|207.158.157.36|US|United States
```
{: .output}

As with the `tail` command, we can also specify how many lines we would 
like to see with `head`. 

Here is an example:
```bash
$ head -n 20 1998.dat
```
```
1998/03/890929468.24864.txt|204.31.253.89|US|United States
1998/03/890929472.24865.txt|153.37.75.113|CN|China
1998/03/890929475.24866.txt|153.37.88.4|CN|China
1998/03/890929479.24867.txt||Fail to get source IP|
1998/03/890929482.24868.txt|153.36.90.245|CN|China
1998/03/890929485.24869.txt|209.84.113.62|US|United States
1998/03/890929489.24870.txt|153.37.97.151|CN|China
1998/03/890929492.24871.txt|198.81.17.36|US|United States
1998/03/890929496.24872.txt|198.81.17.41|US|United States
1998/03/890929499.24873.txt|207.158.157.36|US|United States
1998/03/890929562.24883.txt|208.29.152.2|US|United States
1998/03/890929566.24884.txt|210.61.114.1|TW|Taiwan, Province of China
1998/03/890929569.24885.txt|206.175.229.130|US|United States
1998/03/890929572.24886.txt|207.115.33.46|US|United States
1998/03/890956849.27937.txt||Malformed IP address|
1998/03/891002827.28090.txt||Fail to get source IP|
1998/03/891020025.3222.txt|206.212.231.88|US|United States
1998/03/891020028.3223.txt|206.175.101.79|US|United States
1998/03/891020032.3224.txt|205.232.128.185|US|United States
1998/03/891020035.3225.txt|205.232.128.185|US|United States
```
{: .output}

