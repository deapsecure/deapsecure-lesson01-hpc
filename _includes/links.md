[cc-by-human]: https://creativecommons.org/licenses/by/4.0/
[cc-by-legal]: https://creativecommons.org/licenses/by/4.0/legalcode
[ci]: http://communityin.org/
[coc-reporting]: https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html#reporting-guidelines
[coc]: https://docs.carpentries.org/topic_folders/policies/code-of-conduct.html
[concept-maps]: https://carpentries.github.io/instructor-training/05-memory/
[contrib-covenant]: https://contributor-covenant.org/
[contributing]: {{ repo_url }}/blob/{{ source_branch }}/CONTRIBUTING.md
[cran-checkpoint]: https://cran.r-project.org/package=checkpoint
[cran-knitr]: https://cran.r-project.org/package=knitr
[cran-stringr]: https://cran.r-project.org/package=stringr
[dc-lessons]: http://www.datacarpentry.org/lessons/
[email]: mailto:team@carpentries.org
[github-importer]: https://import.github.com/
[importer]: https://github.com/new/import
[jekyll-collection]: https://jekyllrb.com/docs/collections/
[jekyll-install]: https://jekyllrb.com/docs/installation/
[jekyll-windows]: http://jekyll-windows.juthilo.com/
[jekyll]: https://jekyllrb.com/
[jupyter]: https://jupyter.org/
[lc-lessons]: https://librarycarpentry.org/#portfolio
[lesson-example]: https://carpentries.github.io/lesson-example/
[mit-license]: https://opensource.org/licenses/mit-license.html
[morea]: https://morea-framework.github.io/
[numfocus]: https://numfocus.org/
[osi]: https://opensource.org
[pandoc]: https://pandoc.org/
[paper-now]: https://github.com/PeerJ/paper-now
[python-gapminder]: https://swcarpentry.github.io/python-novice-gapminder/
[pyyaml]: https://pypi.python.org/pypi/PyYAML
[r-markdown]: https://rmarkdown.rstudio.com/
[rstudio]: https://www.rstudio.com/
[ruby-install-guide]: https://www.ruby-lang.org/en/downloads/
[ruby-installer]: https://rubyinstaller.org/
[rubygems]: https://rubygems.org/pages/download/
[styles]: https://github.com/carpentries/styles/
[swc-lessons]: https://software-carpentry.org/lessons/
[swc-releases]: https://github.com/swcarpentry/swc-releases
[workshop-repo]: {{ site.workshop_repo }}
[yaml]: http://yaml.org/

{% comment %}
    Additional items to link.
    These represent my best efforts to get materials that are suitable
    for somewhat competent learners, given that we only have one link
    for each topic.
{% endcomment %}

[what-is-bash]: https://opensource.com/resources/what-bash
[dash]: https://wiki.archlinux.org/title/Dash
[ksh]: https://www.ibm.com/docs/en/aix/7.3?topic=shells-korn-shell
[zsh]: https://zsh.sourceforge.io/Guide/zshguide.html
[tcsh]: https://en.wikipedia.org/wiki/C_shell

[SUSv2-shell-lang]: https://pubs.opengroup.org/onlinepubs/7908799/xcu/shellix.html
[SUSv2]: https://pubs.opengroup.org/onlinepubs/7908799/

[man-grep]: https://man7.org/linux/man-pages/man1/grep.1.html

{% comment %} DeapSECURE-specific links {% endcomment %}

[deapsecure-website]: https://deapsecure.gitlab.io/
[deapsecure-hpc-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson01-hpc/
[deapsecure-bd-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson02-bd/
[deapsecure-ml-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson03-ml/
[deapsecure-nn-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson04-nn/
[deapsecure-crypt-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson05-crypt/
[deapsecure-par-lesson]: https://deapsecure.gitlab.io/deapsecure-lesson06-par/

{% comment %} Portions of the lesson in *this* module {% endcomment %}

[HPC-ep11-shell]: {{page.root}}{% link _episodes/11-shell.md %}
[HPC-ep20-text-pipeline]: {{page.root}}{% link _episodes/20-text-pipeline.md %}
[HPC-ep30-script]: {{page.root}}{% link _episodes/30-script.md %}
