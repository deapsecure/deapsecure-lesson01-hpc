---
title: "Analyzing and Summarizing the Distribution of Spam Origins"
teaching: 15
exercises: 30
questions:
- "How do we analyze and summarize HPC results?"
- "What is the distribution of the origins of spam emails in Spam Archive?"
objectives:
- "Analyze and summarize HPC results using simple UNIX tools."
- "Compute the distribution of countries originating spam emails captured in Spam Archive."
keypoints:
- "Simple UNIX tools such as `cut`, `sort`, `head`, `tail` and `uniq` are helpful to analyze text results."
---
       
## Analyzing Text Tables with Unix Tools


