---
title: "Introduction to High-Performance Computing"
teaching: 10
exercises: 0
questions:
- "What is a High-Performance Computing (HPC) system?"
- "Who uses HPC systems?"
- "Why HPC?"
objectives:
- "Understanding what a supercomputer is"
- "Get a basic understanding of what HPC is"
keypoints:
- "Supercomputers are a collection of smaller computers into one big unit"
- "HPC is used in various domains, pretty much everywhere a computer could be used to solve a problem"
- "HPC helps get results faster than traditional computing."
---

## What is a High-Performance Computer?

![Computers from different generations and types (source: <https://commons.wikimedia.org>)](../fig/wiki_commons_computers.png)
<p>Figure: From top to bottom, left to right: Original 1976 Apple 1, Commodore Amiga 1000, Samsung Galaxy Tab 3, Smart Watch, Summit Supercomputer (images courtesy of <a href=' https://commons.wikimedia.org'>Wikimedia Commons</a>)</p>

A computer is an electronic device capable to carryout calculations of various sorts.
Computers typically process data stored in binary form using a given program or application.
A program or application is a set of instructions to execute on a given data.
A high-performance computer(supercomputer) is a computer with a high
level performance or capability in terms of processing speed, memory
capacity, read/write speeds, in comparison to a general purpose computer.
What makes computers different is mainly their purpose.
In order to fulfill its purpose, a computer has to be equipped with
the right components.
For example, personal computers such as laptops can be used for day to
day tasks such as word processing, video gaming, and watching movies,
for this, they feature a keyboard and a considerably wide screen,
decent processor, and memory of the order of a few gigabytes.
Cellphones are used for calling, taking pictures, texting... for this
reason they feature a connectivity allowing communication, they
feature good quality cameras...
A supercomputer on the other hand is used for big calculations, thus
having a monitor or a camera or a keyboard is irrelevant.
In order to successfully perform their tasks, supercomputers are
composed of a high speed network connecting a set of computing nodes.
Each node is equipped with high-performance processors, and a
high-performance memory.
The nodes come together as a whole to deliver computational speeds of
the orders of a hundred petaflops (10<sup>15</sup> floating-point
operations per second).

## What is high performance computing?

High performance computing (HPC) is the use of supercomputers to solve
large problems from various disciplines such as weather forecasting,
molecular modeling, material sciences, aerodynamics,
cybersecurity... to name a few.
In order to do HPC, you have to have a supercomputer, and a problem worth solving.
In HPC, you are capable to solve problems using more than a single
computing node, in contrast to general purpose computing.

![High performance computing applications](../fig/sim1.jpg)
![High performance computing applications](../fig/sim2.jpg)
![High performance computing applications](../fig/sim3.jpg)
<p align="center">Figure: High performance computing applications(images courtesy of <a href='https://computing.llnl.gov/tutorials/parallel_comp/'>LLNL</a>).</p>

## Why Do We Need an HPC?

Some problems require days of calculations, but are also time sensitive.
Take for example weather forecasting, it would be useless to have a
forecast for yesterday.
Why would this happen?
Weather forecasting requires looking at different models, each taking
into account a number of information such as wind direction, air
humidity, cloud formation...
All this is time consuming and every slight variation from what was
expected may have dire consequences on projected results.
Weather forecaster use several models in order a coverage of most alternatives.
Doing all this in a timely way is primordial.
Some problems are too big to be solved with a general purpose computer.
Computers use data to solve problems.
For this reason, they feature a memory where programs and data to
operate on are stored for the time needed to solve the problem.
The size of the memory of a computer is a big limiting factor on how
much data can be used at a given time.
For a program to be solvable, it has to fit in the computer memory.
Supercomputers combine memories on compute nodes to seamlessly work as
a single computer's memory.
This brings the ability to the computer to fit bigger problems.
Advances in computer architecture also give HPC an advantage.
Current computers feature components of different nature working
together to deliver faster speeds.
This is called heterogeneous computing.
Components such as GPU (Graphics Processing Unit) can be used to
accelerate computations by adding specific programming constructs to
traditional programs.
Concept such as vector processing make this possible.
Tasks that would take a CPU much longer to complete could be done
faster using co-processors or GPUs.

### How Parallel Computing Works
Parallel computing entails the division of computational tasks across 
multiple processing units, enabling the simultaneous processing of 
different data segments. This methodology significantly reduces the 
overall execution time by leveraging the collective computational 
power of several workers.  ![Example for parallel computing](../fig/parallelworks.png) For instance, a task that would ordinarily 
take one worker approximately six hours to complete can be 
accomplished in just one and a half hours when divided among 
four workers. Extending this approach to a larger scale, 
distributing the workload among 400 processors could potentially 
decrease the total runtime to a mere 54 seconds. This illustrates 
the efficiency and scalability that parallel computing offers for 
complex computational tasks.


## The Key to HPC Computing Power

The key to using HPC lies in *parallel processing*.
In a nutshell, parallel computation entails breaking down a set of
computations into many little sub-tasks so that they can be processed
in parallel by the many processing units available in an HPC system.
As we shall learn throughout this training program, parallel processing
is not always straightforward to do.
This is why training programs such as this one will help you to acquire
basic understanding of an HPC and how to program it to solve your problem
faster than your typical desktop or laptop computers.

{% include links.md %}

## What is Cyberstructure?
I still don't find sidebat command.

{: .sidebar}

