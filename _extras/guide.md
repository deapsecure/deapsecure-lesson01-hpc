---
title: "Instructor Notes"
---


## Shell Variants

As a computer program, any command-line shell has two modes of operation:
the *interactive* mode
to receive and run commands typed interactively by its user,
and the *batch* mode
to read the commands from a text file and executes them.
This principle applies to all shells,
including UNIX shells [bash, Korn shell, Z shell, etc] and
Windows shells.
However, there are major variants of shells in use today,
many of which are not compatible with one another.
It is imperative that learners have access to the correct shell,
especially for the scripting part.

This lesson assumes that learners use a modern variant of the Bourne shell
(which includes [bash][what-is-bash], [dash][dash],
[Korn shell][ksh], [Z shell][zsh]) in a Linux or UNIX-like environment.
While this is frequently assumed to be the "UNIX shell",
there is another major flavor in the UNIX world called C shell
(with [tcsh][tcsh] being the prevalent implementation)
that is not compatible with the Bourne shell.
Windows proprietary shells (Command Prompt and PowerShell) are not appropriate
because their syntax and commands are totally different from UNIX shells.
The earlier parts of this lesson (e.g.
[Basic Shell Interac:on][HPC-ep11-shell]
[Text Processing Tools & Pipeline][HPC-ep20-text-pipeline])
do not use Bourne-specific syntax,
therefore any flavor of UNIX shells, including bash and tcsh,
may be used in these earlier episodes.
We specifically use Bash throughout the lesson,
because this is often the default shell in Linux operating system,
which powers the vast majority of the HPC systems today.

Technically speaking, the language constructs adopted in this lesson
are specified in
[Shell Command Language Index][SUSv2-shell-lang]
section of the
[The Single UNIX Specification, Version 2][SUSv2]
released in 1997.
Any shell that implements this language specification will work
for the entire lesson.



{% include links.md %}
