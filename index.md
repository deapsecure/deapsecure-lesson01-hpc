---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

This is the first module of ODU's DeapSECURE cyberinfrastructure training.
This module contains the introduction to High-Performance Computing (HPC)
clusters, complemented with many hands-on exercises.
First, we introduce what an HPC is,
then how to access an HPC system (ODU Wahab cluster).
Next, we will present a crash course, or refresher, on UNIX shell.
We will use the UNIX shell knowledge to write a simple pipeline to
process a very large number of spam emails and obtain some statistical
knowledge about them.
Our final goal in this module is to perform basic analysis on
a massive set of spam emails. 


<!-- > **Where are we going?**
>
> First, we introduce what an HPC is,
> then how to access an HPC system (ODU Turing cluster).
> Next, we will present a crash course, or refresher, on UNIX shell.
> We will use the UNIX shell knowledge to write a simple pipeline to
> process a very large number of spam emails and obtain some statistical
> knowledge about them. -->
<!-- {: .callout} -->

<!-- {% comment %} This is a comment in Liquid {% endcomment %} -->

> ## Prerequisites
>
> * Basic computer interactions, students should know how to interact with a computer using a keyborad.
> * Basic concepts such as directories, files, and paths.
> * Basic text editing skills. Students should know how to input text, issue commands...
>
{: .prereq}

{% include links.md %}
